from django.contrib import admin
from .models import Gato,Localizador,Dueño

admin.site.register(Gato)
admin.site.register(Localizador)
admin.site.register(Dueño)